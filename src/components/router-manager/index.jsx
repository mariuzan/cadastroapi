import { Menu } from "antd";
import { useState } from "react";
import { Link, Route, Switch, useLocation, useHistory } from "react-router-dom";
import Register from "../../pages/register/";

import Key from "../../data/local-storage";

const RouterManager = () => {
  const { allowed } = useState(false);
  const location = useLocation();
  const history = useHistory();
  const path = location.pathname

 

  const cleanStorage =()=>{
    localStorage.authToken=null
    history.push("/")    
    console.log("Storage is cleaned")
  }

  return (
    <>
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={["/"]}
        style={{ lineHeight: "64px" }}
        selectedKeys={[location.pathname]}
      >
        {(((path === "/" ) || (path === "/register" ))) && (<Menu.Item key="/"><Link to="/">Login</Link></Menu.Item>)}
        {(((path === "/" ) || (path === "/register" ))) && (<Menu.Item key="/register"><Link to="/register">Register</Link></Menu.Item>)}
        {((path == "/users" ) ||(path === path && path !== "/"))  && (<Menu.Item onClick={() => { history.push("/users");}} key="/users" > <Link to="/users">Users</Link></Menu.Item>)}
        {(path === "/users" )  && (<Menu.Item onClick={cleanStorage} key="/logoff" > <Link to="/">Logoff</Link></Menu.Item>)}
      </Menu>
      <Switch>
        <Route path="/register" component={Register}>
          <Register />
        </Route>
        <Route path="/logoff"></Route>
        {allowed && <Route path="/users" />}
        <Key></Key>
      </Switch>
    </>
  );
};
export default RouterManager;
