import { useEffect, useState } from "react";
import axios from "axios";
import { Table, Button } from "antd";
import { useHistory, useParams } from "react-router-dom";

const columns = [
  {
    title: "id",
    dataIndex: "id",
    key: "1",
  },
  {
    title: "Name",
    dataIndex: "name",
    key: "2",
  },
  {
    title: "Comment",
    dataIndex: "comment",
    key: "3",
  },

  {
    title: "Grade",
    dataIndex: "grade",
    key: "4",
  },
];


const UserFeedbacks = () => {
  const [postFeedback, setPostFeedback] = useState();
  const history = useHistory();
  const params = useParams();

  const createNewFeedback = () => {
    history.push(`/users/feedbacks/${params.id}/newfeedback`);
  };

  useEffect(() => {
    const token = window.localStorage.getItem("authToken");

    axios
      .get(`https://ka-users-api.herokuapp.com/users/${params.id}/feedbacks`, {
        headers: { Authorization: token },
      })
      .then((results) => {
        setPostFeedback(results.data);
      });
  }, [params, setPostFeedback]);

  return (
    <div>
      <h1>Lista de Feedbacks do Usuário id: {params.id}</h1>
      <Table dataSource={postFeedback} columns={columns} />
      <Button onClick={() => createNewFeedback()}>Novo feedback</Button>
    </div>
  );
};

export default UserFeedbacks;
