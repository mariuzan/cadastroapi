import { Link } from "react-router-dom";
import {useEffect} from "react"
import { Table } from "antd";

const columns = [
  {
    title: "id",
    dataIndex: "id",
    key: "1",
  },
  {
    title: "Name",
    dataIndex: "name",
    key: "2",
  },
  {
    title: "User",
    dataIndex: "user",
    key: "3",
  },

  {
    title: "Email",
    dataIndex: "email",
    key: "4",
  },
  {
    title: "Feedbacks",
    render: (data) => {
      return (
        <Link to={{ pathname: `/users/feedbacks/${data.id}` }}>
          Visitar os feedbacks
        </Link>
      );
    },
  },
];



const Users = ({ list }) => {
 

  useEffect((list) => {
  
  },[{list}])

  return (
    <div>
      <h1>Lista de Usuários</h1>
      <Table dataSource={list} columns={columns} />
    </div>
  );
};
export default Users;
