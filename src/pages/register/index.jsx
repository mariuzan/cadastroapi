import "./index.css";
import { Form, Input, Button } from "antd";
import { useHistory } from "react-router-dom";
import axios from "axios";

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const RegistrationForm = () => {
  const history = useHistory();
  const [form] = Form.useForm();
  const rgx = require("../../data/validator");

  const onFinish = (values) => {
    console.log("Received values of form: ", values);

    axios
      .post("https://ka-users-api.herokuapp.com/users", { user: values })
      .then(function (response) {
        console.log("salvo com sucesso" + response);
        history.push("/");
      });
  };

  return (
    <div className="form-register">
      <h3>Cadastro de Usuário</h3>
      <Form
        {...formItemLayout}
        form={form}
        name="register"
        onFinish={onFinish}
        scrollToFirstError
      >
        <Form.Item
          name="user"
          label={<span>Usuário</span>}
          rules={[
            {
              required: true,
              message: `Por favor insira seu usuário`,
            },
            {
              whitespace: true,
              message: `Seu usuário não pode iniciar com espaços em branco`,
            },
            {
              pattern: rgx.username,
              message: `Somente letras ou números`,
            },
            {
              min: 5,
              message: "Seu usuário deverá possuir ao menos 5 caracteres",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="name"
          label={<span>Nome</span>}
          rules={[
            {
              required: true,
              message: `Por favor insira seu nome`,
              whitespace: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="email"
          label="E-mail"
          rules={[
            {
              type: "email",
              message: `O e-mail não é válido`,
            },
            {
              required: true,
              message: `Por favor, digite seu e-mail`,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="password"
          label="Senha"
          rules={[
            {
              required: true,
              message: `Por favor, insira sua senha!`,
            },
            {
              min: 4,
              message: `Sua senha não pode ter menos que 4 caracteres`,
            },
            {
              max: 10,
              message: `Sua senha não pode ter mais que 10 caracteres`,
            },
            {
              pattern: rgx.password,
              message: `Sua senha deverá possuir no mínimo 1 letra maiúscula, 1 letra minúscula, 1 caracter especial e 1 número`,
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="password_confirmation"
          label="Repita Senha"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: `Por favor, confirme sua senha!`,
            },
            {
              min: 4,
              message: `Sua senha não pode ter menos que 4 caracteres`,
            },
            {
              max: 10,
              message: `Sua senha não pode ter mais que 10 caracteres`,
            },

            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }

                return Promise.reject(`As senhas não são iguais, reveja!`);
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            Cadastrar
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default RegistrationForm;
