import { Form, Input, Button } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import "./index.css";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import { Alert } from "antd";
import { useState} from "react";


const LoginForm = ({setKey}) => {

  const [err, setError] = useState(false);
  const history = useHistory();

  const onFinish = (values) => {
    console.log("Received values of form:", values);
    axios
      .post(`https://ka-users-api.herokuapp.com/authenticate`, { ...values })
      .then((response) => {
        window.localStorage.setItem("authToken", response.data.auth_token);
        setKey(true);
        console.log(response.data.auth_token);
        history.push("/users");
      }).then((response) => {history.push("/users");})
      .catch((err) => setError(true))
      
  };





  return (
    <div className="form-login">
      <h3>Login</h3>
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
      >
        <Form.Item
          name="user"
          rules={[
            {
              required: true,
              message: `Por favor, insira seu usuário!`,
            },
          ]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Nome de usuário"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: `Por favor, insira sua senha!`,
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Senha"
          />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Conectar
          </Button>
        </Form.Item>
        <Form.Item>
          Não possui cadastro? <Link to="/register">cadastre-se agora</Link>
        </Form.Item>
        {err && (
          <Alert message="Usuário ou senha incorretas" type="error" showIcon />
        )}
      </Form>
    </div>
  );
};

export default LoginForm;
