import React from "react";
import { Form, Input, Button } from "antd";
import "./index.css";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";

const FeedbackForm = () => {
  const params = useParams();
  const history = useHistory();

  const onFinish = (values) => {
    const token = window.localStorage.getItem("authToken");

    axios
      .post(
        `https://ka-users-api.herokuapp.com/users/${params.id}/feedbacks`,
        { feedback: { ...values } },
        { headers: { Authorization: token } }
      )
      .then((response) => history.push("/users"));
  };

  return (
    <div className="new-feedback">
      <h3>Novo Feedback para o usuário id: {params.id}</h3>

      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
      >
        <Form.Item
          name="name"
          rules={[
            {
              required: true,
              message: `Por favor, insira seu nome!`,
            },
          ]}
        >
          <Input placeholder="Seu nome" />
        </Form.Item>

        <Form.Item
          name="comment"
          rules={[
            {
              required: true,
              message: `Por favor, insira seu comentário!`,
            },
          ]}
        >
          <Input placeholder="Comentário" />
        </Form.Item>

        <Form.Item
          name="grade"
          rules={[
            {
              required: true,
              message: `Por favor, insira sua nota!`,
            },
          ]}
        >
          <Input placeholder="Nota" />
        </Form.Item>

        <Button type="primary" htmlType="submit" className="login-form-button">
          Enviar
        </Button>
      </Form>
    </div>
  );
};
export default FeedbackForm;
