import { Route, Switch, useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
import LoginForm from "../pages/user-home-login/index";
import Users from "../pages/users/index";
import axios from "axios";
import UserFeedbacks from "../pages/feedbacks";
import FeedbackForm from "../pages/feedback-form"

const Key = () => {
  const [haveKey, setKey] = useState(undefined);
  const [people, setPeople] = useState(undefined);
  const history = useHistory();

  useEffect(() => {
    const token = window.localStorage.getItem("authToken");
    if (!token) {
      setKey(false);
    }
    axios
      .get(`https://ka-users-api.herokuapp.com/users`, {
        headers: { Authorization: token },
      })
      .then((results) => {
        setPeople(results.data);
        setKey(true);
        console.log("testing token " + token);
        history.push("/users");
      })
      .catch(() => {
        setKey(false);
      });
  }, [setPeople, haveKey]);

  if (haveKey === undefined) {
    return <div>Processando...</div>;
  }

  if (haveKey === false) {
    return (
      <Switch>
        <Route path="/">
          <LoginForm setKey={setKey} />
        </Route>
      </Switch>
    );
  }
  return (
    <Switch>
      <Route exact path="/">
        <LoginForm setKey={setKey} />
      </Route>
      <Route exact path="/users">
        <Users list={people} />
      </Route>
      <Route exact path="/users/feedbacks/:id">
        <UserFeedbacks />
      </Route>     
      <Route exact path="/users/feedbacks/:id/newfeedback">
        <FeedbackForm />
      </Route>   
    </Switch>
  );
};

export default Key;


