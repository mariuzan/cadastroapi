import React from "react";
import RouterManager from "./components/router-manager";

import "antd/dist/antd.css";
import "./index.css";
import { Layout } from "antd";

const App = () => {
  return (
    <Layout className="layout">
      <div className="logo" />
      <RouterManager />
    </Layout>
  );
};

export default App;
